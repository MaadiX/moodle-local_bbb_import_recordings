# BigBlueButton import video recordings plugin #
Copyright (C) 2024 [Maadix](https://www.maadix.net/)

## What is? ##
If video recording is enabled in your BBB instance, this plugin can download the video and add it to the course. It also unpublishes the public video in BBB, making it only available to logged-in users from Moodle

## Version support ##
This plugin has been developed to work on Moodle releases 4.1.

## Installation ##
* Enable video recording in your BBB [adding video](https://docs.bigbluebutton.org/development/recording/#deploying-other-formats)
* Copy "bbb_import_recordings" folder into Moodle's "local" folder
* Visit admin page to install module
