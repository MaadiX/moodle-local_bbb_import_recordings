<?php

/**
 * @package   local_bbb_import_recordings
 * @copyright 2024, Maadix
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

use mod_bigbluebuttonbn\instance;
use mod_bigbluebuttonbn\recording;

function local_bbb_import_recordings_execute_task(){

	global $DB,$USER,$CFG;


	require_once($CFG->dirroot . '/course/modlib.php');

	//get recordings
	$recordings = $DB->get_records('bigbluebuttonbn_recordings',['status' => '2']);

	foreach($recordings as $r){
		$datets = $r->timecreated;
                $date = userdate($datets);
		$date_clean = str_replace(array(',', ' '), '-', $date);
                $rid = $r->id;
		$recid = $r->recordingid;
		$rc = $r->courseid;
	        $recording = new recording($rid);
                $url = $recording->get_remote_playback_url('video');
                $url = $url."video-0.m4v";
		$instance = $recording->get_instance();
		$bigbluebuttonbn = $instance->get_instance_data();
                $rpub = $recording->get('published');
		if ($rpub){
			$ispublished = "true";
		}else{
			$ispublished = "false";
		}
		$cm = $instance->get_cm();
                $cmid = $cm->id;
		$course = $instance->get_course();
                $sectionno = $cm->section;

		//section_info id (not section id) needed to create label
		$sectioninfo = get_fast_modinfo($course)->get_section_info_by_id($sectionno);
		$sectioninfoid = $sectioninfo->section;

		//course context
		$context = context_course::instance($rc);
		$contextid = $context->id;

		//course users that can admin bbb, to pick one as user to upload files and labels
		$enusers = get_enrolled_users($context,'mod/bigbluebuttonbn:managerecordings');
		//get first element
		$fuser=reset($enusers);
		$fuserid = $fuser->id;

		//echo "bbb name $bigbluebuttonbn->name / date $date / courseid $rc / module $cmid / user $fuserid / section_info $sectioninfoid / section $sectionno / recording id $rid / published $ispublished / url $url / bbb recordid $recid \n";

		//files API
		$fs = get_file_storage();

		//if reccording is published
		if ($rpub){

			cron_setup_user($fuser);

			$modulename = 'resource';
			list($module, $contextx, $cw, $cmx, $data2) = prepare_new_moduleinfo_data($course, $modulename, $sectioninfoid);

			//upload file
			$bbbname = $bigbluebuttonbn->name;
			$itemid = 0;
			$filename = $date_clean.".m4v";
			$draftcontext = context_user::instance($fuser->id);
			$draftitemid = file_get_submitted_draft_itemid($draftcontext->id);
			$filemanageropts = array('subdirs' => 0, 'maxbytes' => '0', 'maxfiles' => 50, 'context' => $draftcontext);
			file_prepare_draft_area($draftitemid, $draftcontext->id, 'user', 'draft', $itemid, $filemanageropts);

			$file = new stdClass();
			$file->component = 'user';
			$file->contextid = $draftcontext->id;
			$file->filearea = 'draft';
			$file->itemid = $draftitemid;
			$file->filepath = '/';
			$file->filename = $filename;
			$file->timecreated = time();
			$file->timemodified = time();
			$uploaded_file = $fs->create_file_from_url($file, $url);

			$file = $fs->get_area_files($file->contextid, $file->component, $file->filearea, $draftitemid, 'itemid', false);
			$file = reset($file );//  file is there.

			//create file resource
			$uploadinfo = new stdClass();
			$uploadinfo->type = 'Files';
			$uploadinfo->course = $course;
			$uploadinfo->section = $sectioninfoid;
			$uploadinfo->module = $module->id;
			$uploadinfo->modulename= $module->name;
			$uploadinfo->files=$draftitemid;
			$uploadinfo->displayname = "Recording - " .$bbbname. " - " .$date;

			$data = new stdClass();
			$data->showdescription='0';
			$data->printintro='1';
			$data->filterfiles='0';
			$data->downloadcontent='1';
			$data->course = $uploadinfo->course->id;
			$data->name = $uploadinfo->displayname;
			$data->intro = '';
			$data->introformat = FORMAT_HTML;
			$data->section = $uploadinfo->section;
			$data->module =$uploadinfo->module;
			$data->modulename =$uploadinfo->modulename;
			$data->add ='resource';
			$data->return = 0;
			$data->sr = 0;
			$data->files = $uploadinfo->files; 
			$data->visible=1;
			$data->visibleoncoursepage=1;
			$data->display='0';
			$data->completion='1';
			add_moduleinfo($data, $uploadinfo->course);

			//unpublish recording
	                $recording->set('published', false);

			// Break the main loop after first iteration
			break;

		}

	}

}

?>
